"use strict";

let userName,
userAge,
fullAge = 18,
adultAge = 22;

do {
    userName = prompt("What is your name?", userName);
} while (userName === "" || userName === null || isFinite(userName));

do {
    userAge = prompt("How old are you?", userAge);
} while (userAge === null || userAge.trim() === "" || isNaN(+userAge));

if (userAge < fullAge) {
    alert("You are not allowed to visit this website");
} else if(userAge >= fullAge && userAge <= adultAge) {
    confirm("Are you sure you want to continue?");
     alert(`Welcome, ${userName}`);
} else if(userAge > adultAge) {
    alert(`Welcome, ${userName}`);
}

