1. Переменную var можно использовать без её обьявления. Даже в режиме "use strict"
   Example.
   age = 35;
   console.log(age);
   var age;

   let и const выдал бы ошибку

2. область видимости переменных let, const. Переменная внутри блока будет не видна за его пределами.
   Example.
   if (true) {
   let myAge = 25;
   }

   console.log(myAge);
   в таком случае будет ошибка.
   если обьявить через var - то ошибки не будет
   if (true) {
   var myAge = 25;
   }

   console.log(myAge);

3. Переменную let, const видно только после обьявление
   Example.
   console.log(myAge);
   let age = 35;
   будет выдавать ошибку

   console.log(myAge);
   var age = 35;

   все будет работать.
